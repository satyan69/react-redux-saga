import { combineReducers } from 'redux';
import formReducer from './Form/form.reducer';

const rootReducer = combineReducers({
    data: formReducer,
});

export default rootReducer;