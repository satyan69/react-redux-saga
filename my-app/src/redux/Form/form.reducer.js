import {  GET_FORM_DATA_SUCCESS, GET_FORM_DATA_ERROR } from './form.types';

    const INITIAL_STATE = [];
    const reducer = (state = INITIAL_STATE, action) => {
      //debugger
        switch (action.type) {

            case GET_FORM_DATA_SUCCESS:

               return [
                 ...state,
                 action.formData
               ]
               case GET_FORM_DATA_ERROR:

               return {

                 ...state,
                 msg: 'error',

               };

             default: return state;

        }

    };

    export default reducer;