import { REQUEST_DATA } from './form.types';

    export const submitData = (payload) => {
        return {
            type: REQUEST_DATA,
            data: payload

        };

    };