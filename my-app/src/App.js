import React from 'react';
import './App.css';
import Form from './containers/Pages/Form/Form';
import FormList from './containers/Pages/FormList/FormList';

function App() {
  return (
    <div className="App">
      <header>
        <div className="sideBar">
          <FormList/>
          <Form />
        </div>
      </header>
    </div>
  );
}

export default App;
