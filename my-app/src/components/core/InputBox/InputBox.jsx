import React from 'react';
import PropTypes from 'prop-types';

function InputBox(props) {
    return (
        <div className='input'>
            <input
                type={props.type}
                value={props.value}
                name={props.name}
                placeholder={props.placeholder}
                onChange={(e) => props.onChange(e.target.value, e)}
            />
        </div>
    )
}
InputBox.prototypes = {
    type: PropTypes.string,
    value: PropTypes.any,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func
}
InputBox.defaultTypes = {
    type: '',
    value: '',
    name: '',
    placeholder: '',
    onChange: () => {}
}
export default InputBox;