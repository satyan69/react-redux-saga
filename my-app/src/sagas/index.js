import { fork } from 'redux-saga/effects';
import formSaga from './FormSaga';
export default function* rootSaga() {
    yield* [
        fork(formSaga),
    ];
}