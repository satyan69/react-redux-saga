import { put, takeLatest, call } from 'redux-saga/effects';
//import ProductListingApi from '../interfaces/PLP/PLPApi';
import {
    REQUEST_DATA,
    GET_FORM_DATA_SUCCESS,
    GET_FORM_DATA_ERROR
} from '../redux/Form/form.types';

export function* submitFormSaga(action) {
    try {
        //const result = yield call(FactorySaga, ProductListingApi.getProductListing, action);
        if (true) {
            yield put({
                type: GET_FORM_DATA_SUCCESS,
                formData: action.data
            });
        } else {
            yield put({ type: GET_FORM_DATA_ERROR });
        }
    } catch (err) {
        yield put({ type: GET_FORM_DATA_ERROR });
    }
}
export default function* watchProductListRequest() {
    yield takeLatest(REQUEST_DATA, submitFormSaga);
}
