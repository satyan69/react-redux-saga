import React, { useState } from 'react';
import Input from '../../../components/core/InputBox/InputBox';
import { connect } from "react-redux"
import {
    submitData
  } from "../../../redux/Form/form.actions"

function Form(props) {
    const initialState = {
        userName: "",
        shopName: "",
    };
    const [values, setValues] = useState(initialState);
    const [errors, setFormErrors] = useState({});

    const onChangeInput = (inputField, value) => {
        const valuesNew = {};
        if (inputField === 'userName') {
            valuesNew.userName = value;
        } else if (inputField === 'shopName') {
            valuesNew.shopName = value;
        }
        setValues({ ...values, [inputField]: value });
    }
    const handleSubmit = (event) => {
        if (event) event.preventDefault();

        if (errors.userName || errors.shopName) {
            return true;
        }
        const errorsField = {};
        if (!values.userName) {
            errorsField.userName = "Enter an user Name";
        } else {
            errorsField.userName = "";
        }

        if (!values.shopName) {
            errorsField.shopName = "Enter shop Name";
        } else {
            errorsField.shopName = "";
        }
        //setValues({ ...values, userName: values.userName });
        //setValues({ ...values, shopName: values.shopName });
        setValues(initialState);
        setFormErrors(errorsField);
        props.submitData(values);
    };
    return (
        <div>

            <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>Name</label>
                    <Input
                        type='text'
                        name='userName'
                        value={values.userName}
                        placeHolder='User Name'
                        onChange={(value) => {
                            onChangeInput('userName', value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label>Shop Name</label>
                    <Input
                        type='text'
                        name='shopName'
                        value={values.shopName}
                        placeHolder='Shop Name'
                        onChange={(value) => {
                            onChangeInput('shopName', value);
                        }}
                    />
                </div>
                <button
                    className='btn btn-primary'
                    type="submit"
                    name="submit"
                >
                    Submit
                </button>
                {errors.userName}
                {errors.shopName}
            </form>
        </div>
    );
}
const mapDispatchToProps = dispatch => {
    return {
      submitData: (data) => dispatch(submitData(data)),
    }
  }
export default connect(null, mapDispatchToProps)(Form);

