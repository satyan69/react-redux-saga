import React from 'react';
import { connect } from "react-redux";
import _get from 'lodash/get';
import PropTypes from 'prop-types';

function FormList(props) {
    const { data } = props;
    return (
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Shop Name</th>
                </tr>
            </thead>
            <tbody>
                {data.map((item,index) => (
                <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{item.userName}</td>
                    <td>{item.shopName}</td>
                </tr>
                ))
                }
    </tbody>
        </table>
    )
}
FormList.prototypes = {
    data: PropTypes.objectOf(PropTypes.any)
}
FormList.defaultProps = {
    data: []
}
const mapStateToProps = state => {
    //debugger
    return {
        data: _get(state, 'data')
    };
};
export default connect(mapStateToProps, null)(FormList);